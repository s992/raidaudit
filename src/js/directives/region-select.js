(function() {
	"use strict";

	angular.module( "raidAudit.directives" )

	.directive("regionSelect", function() {
		return {
			restrict: "E",
			replace: true,
			scope: {
				regions: "=",
				model: "=",
				change: "="
			},
			template: '<select class="form-control" ng-model="model" ng-options="region.key as region.display for region in regions" required></select>'
		};
	});

})();

