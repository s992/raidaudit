(function() {
	"use strict";

	angular.module( "raidAudit.directives" )

	.directive("armoryLink", [ "$interpolate", function( $interpolate ) {
		return {
			restrict: "E",
			replace: true,
			transclude: true,
			scope: {
				region: "=",
				realm: "=",
				name: "=",
				type: "="
			},
			template: 	'<a ng-href="{{ url }}" target="_blank">' +
							'<span ng-transclude></span>' +
						'</a>',

			link: function( scope ) {
				if( scope.type == "player" || !scope.type ) {
					scope.url = $interpolate("http://{{ region }}.battle.net/wow/en/character/{{ realm }}/{{ name }}/advanced")(scope);
				}
				if( scope.type == "guild" ) {
					scope.url = $interpolate("http://{{ region }}.battle.net/wow/en/guild/{{ realm }}/{{ name }}/")(scope);
				}
			}
		};
	}]);

})();