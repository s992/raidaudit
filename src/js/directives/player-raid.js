(function() {
	"use strict";

	angular.module( "raidAudit.directives" )

	.directive("playerRaid", function() {

		return {
			restrict: "E",
			replace: true,
			scope: {
				raid: "=",
				difficulties: "=",
				progression: "="
			},
			template: 	'<div>' +
							'<div class="row">' +
								'<div class="col-md-12">' +
									'<h4>{{ raid.name }}</h4>' +
								'</div>' +
							'</div>' +
							'<div class="row progression">' +
								'<div class="col-md-3" ng-repeat="difficulty in difficulties">' +
									'<simple-raid-progress raid="progression" difficulty="difficulty"></simple-raid-progress>' +
								'</div>' +
							'</div>' +
						'</div>'
		};
	});

})();