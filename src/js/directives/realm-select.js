(function() {
	"use strict";

	angular.module( "raidAudit.directives" )

	.directive("realmSelect", function() {
		return {
			restrict: "E",
			replace: true,
			scope: {
				realms: "=",
				model: "="
			},
			template: '<select class="form-control" ng-model="model" ng-options="realm.slug as realm.name for realm in realms" required></select>'
		};
	});

})();