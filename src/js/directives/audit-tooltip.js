(function() {
	"use strict";

	angular.module( "raidAudit.directives" )

	.directive( "auditTooltip", function( $compile ) {

		var auditMatches = function( e, severity ) {

			if( e.is(".glyphicon-exclamation-sign") && severity == 2 ) {
				return true;
			}

			if( e.is(".glyphicon-question-sign") && severity == 1 ) {
				return true;
			}

			if( e.is(".glyphicon-ok-sign") && severity == 0 ) {
				return true;
			}

			return false;

		};

		return {
			restrict: "A",
			scope: {
				auditData: "="
			},
			link: function( scope, ele ) {

				if( auditMatches( ele, scope.auditData.severity ) ) {

					var auditResults = $("<div audit audit-data='auditData'></div>"),
						content = $compile(auditResults)(scope);

					ele.popover({
						html: true,
						content: content,
						trigger: "hover",
						title: "Audit Results",
						placement: "top",
						container: "body"
					});

					ele.addClass("active");

				} else {
					ele.addClass("inactive");
				}

			}
		}
	});

})();