(function() {
	"use strict";

	angular.module( "raidAudit.services" )

	.factory( "guildService", [ "$filter", "wowApi",
		function guildService( $filter, wowApi ) {

		var service = {};

		service.getMembers = function( name, realm, level ) {
			return wowApi.guild.members({ name: name, realm: realm }).then(function( results ) {
				return $filter("filter")( results.data.members, { character: { level: 90 } });
			});
		};

		return service;

	}])

})();