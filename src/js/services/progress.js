(function() {
	"use strict";

	angular.module( "raidAudit.services" )

	.factory( "progress", [ "raidConfig", function progress( raidConfig ) {

		function progressionFactory( raid, raidInfo ) {

			var progression = {};

			function Progress() {
				this.name = "";
				this.abbreviation = "";
				this.difficulty = "";
				this.kills = 0;
				this.percentCompleted = 0;
				this.bosses = [];
			}

			Progress.prototype.addBoss = function( name, killCount ) {

				this.bosses.push({ name: name, kills: killCount });

				if( killCount > 0 ) {
					this.kills++;
				}

			};

			Progress.prototype.calculatePercentCompleted = function() {
				this.percentCompleted = Math.floor( ( this.kills / this.bosses.length ) * 100 );
			};

			angular.forEach( raidConfig.getDifficulties(), function( difficulty ) {

				var progress = new Progress();

				progress.name = raidInfo.name;
				progress.abbreviation = raidInfo.abbreviation;
				progress.difficulty = difficulty;

				angular.forEach( raid.bosses, function( boss ) {

					var killsKey = difficulty.toLowerCase() + "Kills",
						kills = boss[ killsKey ];

					if( kills || kills == 0 ) {
						progress.addBoss( boss.name, kills );
					}

				});

				progress.calculatePercentCompleted();
				progression[ difficulty ] = progress;

			});

			return progression;

		}

		return progressionFactory;

	}]);

})();