(function() {
	"use strict";

	angular.module( "raidAudit.config" )

	.provider( "raidConfig", function raidConfig() {

		var config = { 
			raids: [],
			difficulties: [],
			currentTier: ""
		};

		this.setRaids = function( raids ) {
			config.raids = raids;
		};

		this.getRaids = function() {
			return config.raids;
		};

		this.setDifficulties = function( difficulties ) {
			config.difficulties = difficulties;
		};

		this.getDifficulties = function() {
			return config.difficulties;
		};

		this.setCurrentTier = function( currentTier ) {
			config.currentTier = currentTier;
		};

		this.getCurrentTier = function() {
			return config.currentTier;
		};

		this.$get = [function() {
			return this;
		}];

	});

})();