(function() {
	"use strict";

	angular.module( "raidAudit.services" )

	.factory( "batchService", [ "$filter", "wowApi",
		function batchService( $filter, wowApi ) {

		var service = {};

		service.parse = function( importString ) {

			var batch = [],
				nameRealmPairs = importString.split(";");

			if( !nameRealmPairs[ nameRealmPairs.length - 1 ].length ) {
				nameRealmPairs.pop();
			}

			angular.forEach( nameRealmPairs, function( nameRealmPair ) {

				var split = nameRealmPair.split(",");

				if( split.length === 2 ) {

					var name = split[0],
						realm = service.cleanRealmName( split[1] );

					batch.push({ name: name, realm: realm });
					
				}

			});

			return batch;

		};

		service.cleanRealmName = function( realm ) {

			return realm.replace(/(of)(?=[A-Z])/g, "Of")
						.replace(/(?:[^'])([A-Z0-9]+)/g, function( $0, $1 ) {
							return $0[0] + " " + $1;
						})
						.trim();

		};

		return service;

	}])

})();