var gulp = require("gulp"),
	inject = require("gulp-inject");

gulp.task("express", function() {
	var express = require("express"),
		app = express();

	app.use(express.static(__dirname));
	app.listen(4000);
});

gulp.task("watch", function() {
	gulp.watch("./src/scss/*.scss", ["sass"]);
});

gulp.task("sass", function() {
	var sass = require("gulp-sass");

	gulp.src("./src/scss/*.scss")
		.pipe(sass())
		.pipe(gulp.dest("./src/css"));
});

gulp.task("default", ["express", "watch"], function() {

});

gulp.task("build-js", function() {
	return gulp.src("src/js/**/*.js")
			.pipe(gulp.dest("build"));
});

gulp.task("build-html", function() {
	return gulp.src("build/**/*.*")
			.pipe(inject(gulp.src("index.html"), {
				addRootSlash: false,
				ignorePath: "/build/"
			}))
			.pipe(gulp.dest("build"));
});

gulp.task("build", ["build-js"], function(cb) {
	gulp.run("build-html",cb);
});