(function() {
	"use strict";

	angular.module( "raidAudit.config", [] );
	angular.module( "raidAudit.controllers", [] );
	angular.module( "raidAudit.directives", [] );
	angular.module( "raidAudit.services", [] );

	angular.module( "raidAudit", [
		"ngRoute",
		"ngBattleNet",
		"raidAudit.config",
		"raidAudit.controllers",
		"raidAudit.directives",
		"raidAudit.services",
		"raidAudit.filters",
		"ui.bootstrap"
	])

	.config([ "battleNetConfigProvider", function( battleNetConfigProvider ) {
		battleNetConfigProvider.setApiKey("8448xctd7pycvfmm536aggjetp2cgzax");
	}])

	.config([ "raidConfigProvider", function( raidConfigProvider ) {

		raidConfigProvider.setRaids([
			{ key: "msv", abbreviation: "MSV", name: "Mogu'shan Vaults" },
			{ key: "hof", abbreviation: "HoF", name: "Heart of Fear" },
			{ key: "toes", abbreviation: "ToES", name: "Terrace of Endless Spring" },
			{ key: "tot", abbreviation: "ToT", name: "Throne of Thunder" },
			{ key: "soo", abbreviation: "SoO", name: "Siege of Orgrimmar" }
		]);

		raidConfigProvider.setDifficulties([ "LFR", "Flex", "Normal", "Heroic" ]);

		raidConfigProvider.setCurrentTier("soo");

	}])

	.config([ "rateLimiterConfigProvider", function( rateLimiterConfigProvider ) {
		rateLimiterConfigProvider.addLimiter( /api\.battle\.net/, 10, 1100 );
	}])

	.config([ "$httpProvider", function( $httpProvider ) {
		$httpProvider.defaults.useXDomain = true;
		$httpProvider.interceptors.push("rateLimiterInterceptor");
	}])

	.config([ "$routeProvider", function( $routeProvider ) {

		$routeProvider.when("/", {
			templateUrl: "views/home.html",
			controller: "HomeCtrl"
		})
		.otherwise({
			redirectTo: "/"
		});

	}])

})();