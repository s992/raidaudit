(function() {
	"use strict";

	angular.module( "raidAudit.controllers" )

	.controller( "HomeCtrl", [ "$scope", "$q", "$filter", "staticData", "playerService", "guildService", "batchService", "raidConfig", "battleNetConfig",
		function HomeCtrl( $scope, $q, $filter, staticData, playerService, guildService, batchService, raidConfig, battleNetConfig ) {

		$scope.resetImports = function() {
			$scope.imports.individual = { name: null };
			$scope.imports.guild = { name: null };
			$scope.imports.batch = { importString: null };
		};

		$scope.changeRegion = function( region ) {

			battleNetConfig.setDefaultRegion( region );
			$scope.realms = [];
			$scope.loading = true;

			staticData.getRealms(true).then(function( realms ) {
				$scope.realms = realms;
				$scope.doneLoading();
			});

		};

		$scope.setActiveTab = function( tab ) {
			$scope.activeTab = tab;
		};

		$scope.isActiveTab = function( tab ) {
			return $scope.activeTab == tab;
		};

		$scope.isSubmitDisabled = function( form ) {
			return form.$invalid || $scope.currentlyLoading();
		};

		$scope.currentlyLoading = function() {
			return $scope.loading;
		};

		$scope.doneLoading = function() {
			$scope.loading = false;
			$scope.resetImports();
		};

		$scope.shouldShowProgress = function() {
			return !$scope.isActiveTab("manual") && $scope.batchStats.total > 0;
		};

		$scope.failuresExist = function() {
			return $scope.failures && !!$scope.failures.length;
		};

		$scope.clearFailures = function() {
			$scope.failures = [];
		};

		$scope.clearImports = function() {
			$scope.players = [];
			$scope.batchStats.total = 0;
			$scope.resetImports();
			$scope.clearFailures();
		};

		$scope.getCurrentTier = function() {
			return raidConfig.getCurrentTier();
		};

		$scope.toggleDetails = function( row ) {

			var idx = $scope.visibleDetailRows.indexOf( row );

			if( idx === -1 ) {
				$scope.visibleDetailRows.push( row );
			} else {
				$scope.visibleDetailRows.splice( idx, 1 );
			}

		};

		$scope.shouldShowDetails = function( row ) {
			return $scope.visibleDetailRows.indexOf( row ) !== -1;
		};

		$scope.setSortDirection = function() {
			$scope.filterOptions.reverse = $scope.filterOptions.sort == "ilvl.equipped" ? true : false;
		};

		$scope.addPlayer = function( name, realm ) {
			$scope.loading = true;
			$scope.loadPlayer( name, realm ).then(function() {
				$scope.doneLoading();
			});
		};

		$scope.loadPlayer = function( name, realm ) {

			var checkExistingPlayer, existingPlayer;

			name = name || $scope.imports.individual.name;
			realm = realm || $scope.imports.realm;

			checkExistingPlayer = $filter("filter")( $scope.players, { name: name });

			if( checkExistingPlayer.length ) {
				existingPlayer = checkExistingPlayer[0];
			}

			return playerService.getPlayer( name, realm ).then(
				function( player ) {
					if( angular.isDefined( existingPlayer ) ) {
						existingPlayer = player;
					} else {
						$scope.players.push( player );
					}
				},
				function( error ) {
					var prettyName = $filter("titlecase")(name),
						prettyRealm = $filter("titlecase")( realm.replace("-", " ") );

					$scope.failures.push({ name: prettyName, realm: prettyRealm, type: "player" });
				}
			);

		};

		$scope.loadBatch = function( batch ) {

			var players = batchService.parse( batch || $scope.imports.batch.importString );

			$scope.batchStats.loaded = 0;
			$scope.batchStats.total = players.length;

			if( players.length ) {
				$scope.loading = true;

				angular.forEach( players, function( player ) {
					$scope.loadPlayer( player.name, player.realm ).then(function() {
						$scope.batchStats.loaded++;

						if( $scope.batchStats.loaded == $scope.batchStats.total ) {
							$scope.doneLoading();
						}
					});
				});
			}

		};

		$scope.loadGuild = function( name, realm, level ) {

			name = name || $scope.imports.guild.name;
			realm = realm || $scope.imports.realm;
			level = level || 90;

			$scope.batchStats.loaded = 0;
			$scope.loading = true;

			guildService.getMembers( name, realm, level ).then(
				function( members ) {
				
					$scope.batchStats.total = members.length;

					angular.forEach( members, function( member ) {
						$scope.loadPlayer( member.character.name, member.character.realm ).then(function() {
							$scope.batchStats.loaded++;

							if( $scope.batchStats.loaded == $scope.batchStats.total ) {
								$scope.doneLoading();
							}
						});
					});

				},
				function( error ) {
					var prettyName = $filter("titlecase")(name),
						prettyRealm = $filter("titlecase")( realm.replace("-", " ") );
						
					$scope.failures.push({ name: prettyName, realm: prettyRealm, type: "guild" });
				}
			);

		};



		// init code
		$q.all({
			realms: staticData.getRealms(),
			regions: staticData.getRegions(),
			classes: staticData.getClasses(),
			races: staticData.getRaces()
		}).then(function( results ) {
			$scope.realms = results.realms;
			$scope.regions = results.regions;
		});

		$scope.difficulties = raidConfig.getDifficulties();
		$scope.raids = raidConfig.getRaids();
		$scope.loading = false;
		$scope.failures = [];
		$scope.players = [];
		$scope.visibleDetailRows = [];

		$scope.filterOptions = {
			difficulty: "Normal",
			sort: "",
			reverse: false
		};

		$scope.batchStats = {
			loaded: 0,
			total: 0
		};

		$scope.imports = {
			bnetRegion: "us",
			realm: null
		};

		$scope.resetImports();
		$scope.setActiveTab("manual");
		$scope.$watch( "imports.bnetRegion", $scope.changeRegion );
		$scope.addPlayer("gewd","kiljaeden");

	}]);

})();