(function() {
	"use strict";

	angular.module( "raidAudit.directives" )

	.directive("raidProgress", function() {
		return {
			restrict: "E",
			replace: true,
			scope: {
				raid: "=",
				difficulty: "="
			},
			template: 	'<div class="progress main">' +
							'<span class="glyphicon left glyphicon-chevron-left" ng-click="changeDifficulty(-1)"></span>' +
							'<div class="progress-bar {{ getProgressClass() }}" style="width: {{ currentRaid.percentCompleted }}%">' +
								'<span>{{ currentRaid.abbreviation }} {{ isolateDifficulty }} ({{ currentRaid.kills }}/{{ currentRaid.bosses.length }})</span>' +
							'</div>' +
							'<span class="glyphicon right glyphicon-chevron-right" ng-click="changeDifficulty(1)"></span>' +
						'</div>',

			link: function( scope ) {

				var difficulties = Object.keys( scope.raid );

				function init() {
					scope.currentRaid = scope.raid[ scope.isolateDifficulty ];
				}

				scope.getProgressClass = function() {

					if( scope.currentRaid.percentCompleted == 100 ) {
						return "progress-done";
					}

					if( 50 < scope.currentRaid.percentCompleted && scope.currentRaid.percentCompleted < 100 ) {
						return "progress-50todone";
					}

					return "progress-0to50";
				};

				scope.changeDifficulty = function( step ) {

					var currentIdx = difficulties.indexOf( scope.isolateDifficulty ),
						newIdx = currentIdx + step;

					if( newIdx > difficulties.length - 1 ) {
						newIdx = 0;
					}

					if( newIdx < 0 ) {
						newIdx = difficulties.length - 1;
					}

					scope.isolateDifficulty = difficulties[ newIdx ];
					init();

				};

				scope.$watch( "difficulty", function( current, prev ) {
					if( current != prev ) {
						scope.isolateDifficulty = scope.difficulty;
						init();
					}
				});

				scope.isolateDifficulty = scope.difficulty;
				init();

			}
		};
	});

})();