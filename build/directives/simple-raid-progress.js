(function() {
	"use strict";

	angular.module( "raidAudit.directives" )

	.directive("simpleRaidProgress", function() {

		function getTooltipProgress( raid ) {

			var ul = $("<ul></ul>", { class: "progress-tooltip" });

			angular.forEach( raid.bosses, function( boss ) {

				var li = $("<li></li>"), 
					displayedKills = boss.kills < 10 ? "&nbsp;&nbsp;" + boss.kills : boss.kills; // don't judge me
				
				li.html( displayedKills + " x " + boss.name );

				if( boss.kills == 0 ) {
					li.addClass("unkilled");
				}

				ul.append(li);

			});

			return ul;

		}

		return {
			restrict: "E",
			replace: true,
			scope: {
				raid: "=",
				difficulty: "="
			},
			template: 	'<div class="progress simple-progress">' +
							'<div class="progress-bar {{ getProgressClass() }}" style="width: {{ currentRaid.percentCompleted }}%;">' +
								'<span>{{ difficulty }} ({{ currentRaid.kills }}/{{ currentRaid.bosses.length }})</span>' +
							'</div>' +
						'</div>',

			link: function( scope, ele ) {

				scope.currentRaid = scope.raid[ scope.difficulty ];

				ele.popover({
					content: getTooltipProgress( scope.currentRaid ),
					html: true,
					placement: "top",
					container: "body",
					trigger: "hover"
				});

				scope.getProgressClass = function() {

					if( scope.currentRaid.percentCompleted == 100 ) {
						return "progress-done";
					}

					if( 50 < scope.currentRaid.percentCompleted && scope.currentRaid.percentCompleted < 100 ) {
						return "progress-50todone";
					}

					return "progress-0to50";
				};

			}
		};
	});

})();