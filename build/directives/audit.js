(function() {
	"use strict";

	angular.module( "raidAudit.directives" )

	.directive( "audit", function() {
		return {
			restrict: "A",
			scope: {
				auditData: "="
			},
			template: 	'<span ng-if="auditData.severity == 0">All good!</span>' +
						'<ul ng-if="auditData.severity > 0">' +
						'	<li ng-if="auditData.missingEnchants">Missing {{ auditData.missingEnchants }} enchant(s).</li>' +
						'	<li ng-if="auditData.emptySockets">{{ auditData.emptySockets }} empty socket(s).</li>' +
						'	<li ng-if="auditData.missingGlyphs">{{ auditData.missingGlyphs }} missing glyph(s).</li>' +
						'	<li ng-if="auditData.missingTalents">{{ auditData.missingTalents }} missing talent(s).</li>' +
						'	<li ng-if="auditData.inappropriateArmorType">{{ auditData.inappropriateArmorType }} item(s) with inappropriate armor type.</li>' +
						'	<li ng-if="auditData.lowLevelItems">{{ auditData.lowLevelItems }} low level item(s).</li>' +
						'</ul>'
		};
	});

})();