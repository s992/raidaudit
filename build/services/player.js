(function() {
	"use strict";

	angular.module( "raidAudit.services" )

	.factory( "playerService", [ "$q", "$filter", "staticData", "wowApi", "progress", "raidConfig",
		function playerService( $q, $filter, staticData, wowApi, progress, raidConfig ) {

		var service = {};

		service.getPlayer = function( name, realm, fields ) {
			fields = fields || [ "audit", "progression", "items", "talents" ];
			return wowApi.character.profile({ name: name, realm: realm, fields: fields }).then( service.transform );
		};

		service.transform = function( response ) {
			return $q.all({
				classes: staticData.getClasses(),
				races: staticData.getRaces()
			}).then(function( results ) {

				var data = response.data,
					classes = results.classes,
					races = results.races,
					player = {},
					trySpec, tryClass, tryRace;

				trySpec = $filter("filter")( data.talents, { selected: true });
				tryClass = $filter("filter")( classes, { id: data.class });
				tryRace = $filter("filter")( races, { id: data.race });

				player.name = data.name;
				player.realm = data.realm;
				player.classID = data.class;
				player.class = tryClass.length ? tryClass[0].name : "Unknown";
				player.currentSpec = trySpec.length ? trySpec[0].spec.name : "Unknown";
				player.race = tryRace.length ? tryRace[0].name : "Unknown";
				player.specs = service.getSpecs( data.talents );
				player.progression = service.getProgression( data.progression.raids );
				player.audit = service.getAudit( data.audit );

				player.ilvl = {
					equipped: data.items.averageItemLevelEquipped,
					overall: data.items.averageItemLevel
				};

				return player;

			});
		};

		service.getSpecs = function( talents ) {

			var specs = [];

			angular.forEach( talents, function( talent ) {
				specs.push( talent.spec ? talent.spec.name : "None" );
			});

			return specs;

		};

		service.getProgression = function( playerRaids ) {

			var progression = {};

			angular.forEach( raidConfig.getRaids(), function( raid ) {

				var raidProgress = $filter("filter")( playerRaids, { name: raid.name })[0];

				progression[ raid.key ] = progress( raidProgress, raid );

			});

			return progression;

		};

		service.getAudit = function( playerAudit ) {

			var audit = {};

			audit.missingEnchants = Object.keys( playerAudit.unenchantedItems ).length;
			audit.emptySockets = playerAudit.emptySockets;
			audit.missingGlyphs = playerAudit.emptyGlyphSlots;
			audit.missingTalents = playerAudit.unspentTalentPoints;
			audit.inappropriateArmorType = Object.keys( playerAudit.inappropriateArmorType ).length;
			audit.lowLevelItems = Object.keys( playerAudit.lowLevelItems ).length;
			audit.severity = service.getAuditSeverity( audit );

			return audit;

		};

		service.getAuditSeverity = function( audit ) {

			var severity = 0,
				highSeverityItems = [ "missingEnchants", "emptySockets", "missingTalents", "inappropriateArmorType", "lowLevelItems" ];

			if( audit.missingGlyphs ) {
				severity = 1;
			}

			angular.forEach( highSeverityItems, function( item ) {
				if( audit[ item ] ) {
					severity = 2;
				}
			});

			return severity;

		};

		return service;

	}])

})();