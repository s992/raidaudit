(function() {
	"use strict";

	angular.module( "raidAudit.services" )

	.provider( "rateLimiter", [ "$interval", "$window", function rateLimiter( $interval, $window ) {

		

	}])

	.factory("rateLimiter", [ "$q", "$interval", "$window", function rateLimiter( $q, $interval, $window ) {

		function RateLimit( pattern, requestLimit, timeLimit ) {
			this.pattern = pattern;
			this.requestLimit = requestLimit;
			this.timeLimit = timeLimit || 1000;
			this.queue = [];
			this.count = 0;
			this.lastRequest = undefined;
			this.currentRequest = undefined;
			this.interval = undefined;
		}

		RateLimit.prototype.add = function( config, deferred ) {
			this.queue.push({ config: config, deferred: deferred });
		};

		RateLimit.prototype.remove = function( index ) {
			this.queue.splice( index, 1 );
		};

		RateLimit.prototype.startProcessing = function() {
			if( !angular.isDefined( this.interval ) ) {
				this.interval = $interval( this.processQueue.bind(this), this.timeLimit );
			}
		};

		RateLimit.prototype.stopProcessing = function() {
			if( angular.isDefined( this.interval ) ) {
				$interval.cancel( this.interval );
				this.interval = undefined;
			}
		};

		RateLimit.prototype.processQueue = function() {

			if( this.queue.length ) {

				for( var i = 0; i < this.requestLimit; i++ ) {

					var request = this.queue[i];

					if( request ) {
						this.process( request, i );
					}

				}

			} else {
				this.stopProcessing();
			}

		};

		RateLimit.prototype.process = function( request, index ) {

			var timeSinceLastRequest;

			this.count++;
			this.currentRequest = $window.performance.now();

			if( !angular.isDefined( this.lastRequest ) ) {
				this.lastRequest = this.currentRequest;
			}

			timeSinceLastRequest = this.currentRequest - this.lastRequest;

			if( timeSinceLastRequest >= this.timeLimit ) {
				this.count = 0;
			}

			if( this.count < this.requestLimit ) {
				request.deferred.resolve( request.config );
				this.lastRequest = this.currentRequest;
				this.remove( index );
			} else {
				this.startProcessing();
			}

		};

		// we get 10 requests per second, but let's give it 1.1 seconds just to account for JS timing weirdness
		var rateLimiter = new RateLimit( /api\.battle\.net/, 10, 1100 );

		return {

			request: function( config ) {

				var deferred = $q.defer();

				if( config.url.match( rateLimiter.pattern ) ) {
					rateLimiter.add( config, deferred );
					rateLimiter.processQueue();
				} else {
					deferred.resolve( config );
				}

				return deferred.promise;

			}
		};

	}]);

})();