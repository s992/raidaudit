(function() {
	"use strict";

	angular.module( "raidAudit.services" )

	.factory( "staticData", [ "$q", "$parse", "wowApi", function staticData( $q, $parse, wowApi ) {

		var service = {},
			cache = {};

		function promise( data ) {
			var deferred = $q.defer();
			deferred.resolve( data );

			return deferred.promise;
		}
		
		cache.classes = [];
		cache.races = [];
		cache.realms = [];
		cache.regions = [
			{ key: "eu", display: "EU" },
			{ key: "kr", display: "KR" },
			{ key: "tw", display: "TW" },
			{ key: "us", display: "US" }
		];

		cache.parse = function( scope, expression ) {
			return $parse( expression )( scope );
		};

		cache.get = function( key, data, dataPath, reload ) {

			if( !cache[ key ].length || reload ) {
				return data().then(function( response ) {
					cache[ key ] = dataPath ? cache.parse( response, dataPath ) : response.data;
					return cache[ key ];
				});
			}

			return promise( cache[ key ] );

		};

		service.getClasses = function() {
			return cache.get( "classes", wowApi.data.characterClasses, "data.classes" );
		};

		service.getRaces = function() {
			return cache.get( "races", wowApi.data.characterRaces, "data.races" );
		};

		service.getRealms = function( reload ) {
			return cache.get( "realms", wowApi.realmStatus, "data.realms", reload );
		};

		service.getRegions = function() {
			return promise( cache.regions );
		};

		return service;

	}])

})();